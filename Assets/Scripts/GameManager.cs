﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockClass
{
	public int[,] blockMap;
	public GameObject blockParent;
}

public class GameManager : SingletonMonoBehaviour<GameManager> {

	[SerializeField]
	GameObject tile;
	[SerializeField]
	GameObject backGroundTilePrefab;
	[SerializeField]
	GameObject blackTilePrefab;
	[SerializeField]
	GameObject targetTilePrefab;
	[SerializeField]
	Canvas canvas;
	[SerializeField]
	Text remainTimeText;
	[SerializeField]
	Text scoreText;
	[SerializeField]
	Text gameOverText;
	[SerializeField]
	Text clearText;
	[SerializeField]
	Text hiscoreText;
	[SerializeField]
	Text missText;

	const int blockSize = 3;
	//int blockCount = 9;

	//LOCAL VARIABLES
	int score = 0;
	int highScore = 0;
	int remainTime = 30;
	int level = 1;
	GameObject backGroundParent = null;

	//GLOBAL VARIABLES
	public string targetBlockName = "";
	public enum GameState{
		None = -1,
		WaitForStart,
		GameStart,
		StageStart,
		StageClear,
		GameOver	
	}

	public GameState currentState = GameState.None;
	public GameState nextState = GameState.None;

	// Use this for initialization
	void Start () {
		CameraSetup ();

		nextState = GameState.WaitForStart;
	}

	void updateHighScore()
	{
		try{
			highScore = PlayerPrefs.GetInt("highscore");
		}
		catch{
			highScore = 0;
		}
	}

	void CameraSetup(){
		//Camera cam = gameObject.GetComponent<Camera> ();
		Camera cam = Camera.main;

		// 理想の画面の比率
		float targetRatio = 9f / 16f;	
		// 現在の画面の比率
		float currentRatio = Screen.width * 1f / Screen.height;
		// 理想と現在の比率
		float ratio = targetRatio / currentRatio;

		//カメラの描画開始位置をX座標にどのくらいずらすか
		float rectX = (1.0f - ratio) / 2f;
		//カメラの描画開始位置と表示領域の設定
		cam.rect = new Rect (rectX, 0f, ratio, 1f);
	}
		
	// Update is called once per frame
	void Update () {

		if (nextState != GameState.None) {
		
			switch (nextState) {

			case GameState.WaitForStart:
				//タイトル画面、チュートリアル表示
				gameOverText.enabled = false;
				clearText.enabled = false;
				missText.enabled = false;

				updateHighScore ();

				break;
			
			case GameState.GameStart:
				remainTime = 30;

				canvas.enabled = false;
				StartCoroutine (GameStartSequence ());
				StartCoroutine (Timer ());
				break;

			case GameState.StageStart:
				//clearText.enabled = false; Coroutineで消す

				StageStartSequence ();
				break;

			case GameState.StageClear:
				//Show Button to Next Stage

				break;
			
			case GameState.GameOver:
				StartCoroutine (GameOverSequence ());
				//Show GameOver and BackToTitle

				break;

			default:
				break;
			}

			currentState = nextState;
			nextState = GameState.None;
		}

		scoreText.text = score.ToString ();
		hiscoreText.text = highScore.ToString ();
		remainTimeText.text = remainTime.ToString ();
	}

	IEnumerator Timer()
	{
		while (true) {
			yield return new WaitForSeconds (1);
			remainTime--;
			if (remainTime <= 0) {
				remainTime = 0;
				nextState = GameState.GameOver;
			}
		}
	}

	IEnumerator ClearSequence()
	{
		clearText.enabled = true;
		nextState = GameState.StageStart;
		yield return new WaitForSeconds (2);
		clearText.enabled = false;
	}

	IEnumerator MissSequence()
	{
		missText.enabled = true;
		remainTime -= 10;
		if (remainTime <= 0) {
			remainTime = 0;
			nextState = GameState.GameOver;
		} else {
			nextState = GameState.StageStart;
		}
		yield return new WaitForSeconds (2);
		missText.enabled = false;
	}

	IEnumerator GameOverSequence()
	{
		int high = PlayerPrefs.GetInt ("highscore");
		if(high < score)
		{
			PlayerPrefs.SetInt ("highscore", score);
			PlayerPrefs.Save ();
		}

		gameOverText.enabled = true;
		yield return new WaitForSeconds (2);
		//nextState = GameState.WaitForStart;
		UnityEngine.SceneManagement.SceneManager.LoadScene("main");
	}

	public void buttonStartClick()
	{
		if (currentState == GameState.WaitForStart) {
			nextState = GameState.GameStart;
		}
	}

	//ブロックが置かれたときの判定
	public void OnBlockDrop(bool isCorrect)
	{
		if (currentState != GameState.StageStart) {
			return;
		}

		//正解
		if (isCorrect) {
			score += level * 10;
			//level = (int)Mathf.Clamp (level++, 0f, 12f);
			level++;
			if (level > 12) {
				level = 12;
			}


			//nextState = GameState.StageClear;
			//nextState = GameState.StageStart;
			StartCoroutine(ClearSequence());

		} else {
			//ミス
			// missの文字を表示して時間を減らす
			StartCoroutine(MissSequence());

			//nextState = GameState.GameOver;
		}

	}

	IEnumerator GameStartSequence()
	{
		MakeBackGround ();
		score = 0;
		level = 2;
		yield return null;	//Return Update() to Enable change status
		nextState = GameState.StageStart;
	}

	List<BlockClass> blockList;
	GameObject targetParent;

	void StageStartSequence()
	{
		if (blockList != null) {
			foreach (BlockClass bc in blockList) {
				Destroy (bc.blockParent);			
			}
		}

		if (targetParent != null) {
			Destroy (targetParent);
		}

		MakeStage ();
		//nextState = GameState.StageStart;
	}
		
	void MakeStage()
	{
		blockList = CreateBlockSet ();

		int x = 0;
		int y = 2;

		int k = 0;
		foreach (BlockClass b in blockList) {
			k++;
			b.blockParent.transform.position = new Vector3 (x, y, 0);
			x += 4;

			//一列に３つ固定で
			if (k % 3 == 0) {
				y += 4;
				x = 0;
			}
		}

		// dicide target block
		int targetIndex = Random.Range(0,blockList.Count);
		BlockClass targetBlock = blockList [targetIndex];
		//GLOBAL
		targetBlockName = targetBlock.blockParent.name;

		targetParent = new GameObject ("TargetBlock");
		targetParent.transform.localScale = new Vector3 (blockSize, blockSize, 0);

        //お題ブロックの作成
        //正解ブロックの黒い部分を使ってブロックを作る
		int[,] taregtMap = targetBlock.blockMap;
		for (int i = 0; i < blockSize; i++) {
			for (int j = 0; j < blockSize; j++) {

				if (taregtMap [i, j] == 0) {
					var block = Instantiate (targetTilePrefab, new Vector2 (i, j), Quaternion.identity);
					block.transform.SetParent (targetParent.transform);
					block.GetComponent<SpriteRenderer> ().sortingLayerName = "Player";
				}
			}
		}
		//targetParent.layer = LayerMask.NameToLayer ("Player");
		targetParent.transform.position = new Vector3(4f,19.5f,0);

		float offset = (float)(blockSize - 1) / (blockSize * 2f);	//がんばった計算式
		targetParent.AddComponent<BoxCollider2D> ().offset = new Vector2 (offset, offset);
		targetParent.GetComponent<BoxCollider2D> ().isTrigger = true;
		targetParent.AddComponent<DragMove> ();
		targetParent.AddComponent<Rigidbody2D> ().gravityScale = 0f;	//Collsion発生のためには少なくとも片方にRidgidBodyが必要
	}

	void MakeBackGround()
	{
		// Make Background
		backGroundParent = new GameObject ("BackGroundParent");
		for (int i = -2; i < 13; i++) {
			for (int j = 0; j < 18; j++) {
				var obj = Instantiate (backGroundTilePrefab, new Vector2 (i, j), Quaternion.identity);
				obj.transform.SetParent (backGroundParent.transform);
			}
		}
	}
		
	List<BlockClass> CreateBlockSet()
	{
		blockList = new List<BlockClass>();

		int i = 0;

		do{
			BlockClass blockClass = new BlockClass();

			int[,] map = null;
			GameObject blockParent = null;
			do {
				if(map != null){map=null;}
				if(blockParent != null){blockParent=null;}

				map = new int[blockSize,blockSize];
				blockParent = createBlock("B" + i.ToString(),out map);
			}
			while(
				isEmptyMap(map)	//継続条件（空っぽの時）
			);

			blockClass.blockMap = map;
			blockClass.blockParent = blockParent;

			deleteSameMap(map,blockList);
			blockList.Add(blockClass);
			i++;

		}
		while(
			blockList.Count < level	//継続条件（指定ブロック数を満たさない）
		);

		return blockList;
	}
		
	void deleteSameMap(int[,] map,List<BlockClass> blockList)
	{
		for (int i = 0; i < blockList.Count; i++) {
			if(isSameMap(map,blockList[i]))
				{
					GameObject.Destroy (blockList [i].blockParent);
					blockList.Remove (blockList [i]);
				}
		}
	}
		
	bool isSameMap(int[,] map,BlockClass bc)
	{
		bool isSame = true;

		int[,] a = map;
		int[,] b = bc.blockMap;

		for (int i = 0; i < blockSize; i++) {
			for (int j = 0; j < blockSize; j++) {
				if (a [i, j] != b [i, j]) {
					isSame = false;
					break;
				}
			}
		}
		return isSame;
	}

	bool isEmptyMap(int[,] a)
	{
		bool isEmpty = true;
		for (int i = 0; i < blockSize; i++) {
			for (int j = 0; j < blockSize; j++) {
				if (a [i, j] > 0) {
					isEmpty = false;
					break;
				}
			}
		}
		return isEmpty;
	}

	GameObject createBlock(string name,out int[,] retMap)
	{
		int[,] map = new int[blockSize,blockSize];

		GameObject parentObject = new GameObject (name);
		parentObject.transform.localScale = new Vector3 (blockSize, blockSize, 0);

		do {
			foreach(Transform tsf in parentObject.transform)
			{
				Destroy(tsf.gameObject);
			}

			for (int i = 0; i < blockSize; i++) {
				for (int j = 0; j < blockSize; j++) {

					int rand = Random.Range (0, 2);

					if (rand == 0) {
						GameObject t = (GameObject)Instantiate (tile, new Vector2 (i, j), Quaternion.identity);
						t.transform.SetParent (parentObject.transform);
						map [i, j] = 1;
					}
					else
					{
						GameObject t = (GameObject)Instantiate (blackTilePrefab, new Vector2 (i, j), Quaternion.identity);
						t.transform.SetParent (parentObject.transform);
						map [i, j] = 0;
					}
				}
			}
		} while(isEmptyMap (map));

		float offset = (float)(blockSize - 1) / (blockSize * 2f);	//がんばった計算式

		parentObject.AddComponent<BoxCollider2D> ();
		parentObject.GetComponent<BoxCollider2D> ().offset = new Vector2 (offset, offset);

		/*
		Vector2 objectSize = gameObject.GetComponent<RectTransform> ().sizeDelta;
		BoxCollider2D collider = GetComponent<BoxCollider2D> ();
		collider.size = objectSize;
		*/


		retMap = map;
		return parentObject;
	}
}
