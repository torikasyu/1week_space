﻿using UnityEngine;

public class DragMove : MonoBehaviour
{

	Vector3 diff = Vector3.zero;

	enum CollisionStatus
	{
		None = -1,
		Target,
		Other
	}

    bool enterTarget = false;
    bool enterOther = false;

	[SerializeField]
	CollisionStatus colStatus = CollisionStatus.None;

    Vector3 mousePointScreen = Vector3.zero;
    Vector3 clickPointScreen = Vector3.zero;
    Vector3 targetPointWorld = Vector3.zero;

    void OnMouseDrag()
	{
        /*
		Vector3 objectPointInScreen = Camera.main.WorldToScreenPoint(this.transform.position);

		Vector3 mousePointInScreen = new Vector3(Input.mousePosition.x,
			Input.mousePosition.y,
			objectPointInScreen.z);

		Vector3 mousePointInWorld = Camera.main.ScreenToWorldPoint(mousePointInScreen);
		mousePointInWorld.z = this.transform.position.z;

		this.transform.position = mousePointInWorld;
*/

        //Vector3 mousePointScreen = Input.mousePosition;
        mousePointScreen = AppUtil.GetTouchPosition();
        targetPointWorld = Camera.main.ScreenToWorldPoint (mousePointScreen);

		targetPointWorld = targetPointWorld + diff;
		targetPointWorld.z = 0;

		this.transform.position = targetPointWorld;
	}
		
	void OnMouseDown()
	{
		Debug.Log ("click");

        //Vector3 clickPointScreen = Input.mousePosition;
        clickPointScreen = AppUtil.GetTouchPosition();
        Vector3 clickPointWorld = Camera.main.ScreenToWorldPoint (clickPointScreen);
		diff = gameObject.transform.position - clickPointWorld;
	}

	void OnMouseUp()
	{
        /*
		if (colStatus == CollisionStatus.Target) {
			Debug.Log ("Collect!!");
			GameManager.Instance.OnBlockDrop (true);
		} else if (colStatus == CollisionStatus.Other) {
			Debug.Log ("Miss!!");
			GameManager.Instance.OnBlockDrop (false);
		}
        */

        if(enterTarget) //正解マスに引っかかっていたら、ミスのマスに引っかかっていても正解とする
        {
            GameManager.Instance.OnBlockDrop(true);
        }
        else if (enterOther)
        {
            GameManager.Instance.OnBlockDrop(false);
        }
    }

	void OnTriggerEnter2D(Collider2D col) {

        /* 謎
		if (GameManager.Instance.targetBlockName == col.gameObject.name) {
			print ("target hit");
		}
        */

        if (GameManager.Instance.targetBlockName == col.gameObject.name)
        {
            enterTarget = true;
        }
        else
        {
            enterOther = true;
        }
    }

    void OnTriggerStay2D(Collider2D col){

        /*
		if (GameManager.Instance.targetBlockName == col.gameObject.name) {
			colStatus = CollisionStatus.Target;
		} else {
			colStatus = CollisionStatus.Other;
		}
        */

        if (GameManager.Instance.targetBlockName == col.gameObject.name)
        {
            enterTarget = true;
        }
        else
        {
            enterOther = true;
        }
    }

    void OnTriggerExit2D()
	{
        //colStatus = CollisionStatus.None;
        enterTarget = false;
        enterOther = false;
	}
}
		